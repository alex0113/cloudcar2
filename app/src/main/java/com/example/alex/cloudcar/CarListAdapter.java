package com.example.alex.cloudcar;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CarListAdapter extends RecyclerView.Adapter<CarListAdapter.CustomViewHolder> {
    private List<Car> feedItemList;
    private Context mContext;

    public CarListAdapter(Context context, List<Car> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_car, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        Car feedItem = feedItemList.get(i);

        customViewHolder.textViewLicense.setText(feedItem.getLicense());
        customViewHolder.textViewName.setText(feedItem.getName());



        //Setting text view title
       // custo
    }

    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;
        protected TextView textViewName;
        protected TextView textViewLicense;

        public CustomViewHolder(View view) {
            super(view);
            this.textViewName = (TextView) view.findViewById(R.id.editTextName);
            this.textViewLicense = (TextView) view.findViewById(R.id.editTextLicense);
        }
    }
}