package com.example.alex.cloudcar;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

public class FeaturesActivity extends AppCompatActivity {

    private final int REQ_CODE_SPEECH_INPUT = 100;
    private static Context context;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            String spokenText = results.get(0);
            Toast.makeText(getApplicationContext(),
                    spokenText,
                    Toast.LENGTH_SHORT).show();

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FeaturesActivity.context = this.getApplicationContext();
        setContentView(R.layout.activity_features);
        Button services_button = (Button) findViewById(R.id.button4);
        services_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(FeaturesActivity.this, ServicesActivity.class);
                FeaturesActivity.this.startActivity(myIntent);
            }
        });

        Button wash_button = (Button)findViewById(R.id.button5);
        wash_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FeaturesActivity.this, WashActivity.class);
                FeaturesActivity.this.startActivity(intent);
            }
        });
        Button drive_button = (Button )findViewById(R.id.button7);
        drive_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpeechRecognizer recognizer =  SpeechRecognizer.createSpeechRecognizer(FeaturesActivity.context);
                /*recognizer.setRecognitionListener(new RecognitionListener() {
                    @Override
                    public void onReadyForSpeech(Bundle bundle) {
                        Toast.makeText(getApplicationContext(),
                                "readyyy",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onBeginningOfSpeech() {
                        Toast.makeText(getApplicationContext(),
                                "Beginning of speech",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onRmsChanged(float v) {

                    }

                    @Override
                    public void onBufferReceived(byte[] bytes) {
                        Toast.makeText(getApplicationContext(),
                                "buffer received",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onEndOfSpeech() {
                        Toast.makeText(getApplicationContext(),
                                "End of speech",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(int i) {
                        Toast.makeText(getApplicationContext(),
                                "Error" + new Integer(i).toString(),
                                Toast.LENGTH_SHORT).show();
                        // Here, thisActivity is the current activity

                        if (checkSelfPermission(Manifest.permission.INTERNET) == 0)
                            ActivityCompat.requestPermissions(FeaturesActivity.this,
                                    new String[]{Manifest.permission.RECORD_AUDIO},
                                    1);
                        else {
                            Toast.makeText(getApplicationContext(),
                                    "has_permissions",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onResults(Bundle bundle) {
                        Toast.makeText(getApplicationContext(),
                                "results",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPartialResults(Bundle bundle) {
                        Toast.makeText(getApplicationContext(),
                                "partial results",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onEvent(int i, Bundle bundle) {
                        Toast.makeText(getApplicationContext(),
                                "event",
                                Toast.LENGTH_SHORT).show();
                    }
                });
                */


                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
                intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getApplication().getPackageName());
                try {
                    startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
                } catch (ActivityNotFoundException a) {
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.speech_not_supported),
                            Toast.LENGTH_SHORT).show();
                }
                recognizer.startListening(intent);


            }
        });


    }
}
