package com.example.alex.cloudcar;

import android.Manifest;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.speech.SpeechRecognizer;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.R.attr.data;

public class MainActivity extends AppCompatActivity {

    private LinearLayoutManager mLayoutManager;
    private CarListAdapter mAdapter;
    private LayoutInflater inflater;
    private static int count = 0;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        final LinearLayout cars = (LinearLayout) findViewById(R.id.linear_layout);
        inflater = LayoutInflater.from(MainActivity.this); // 1
        View theInflatedView = inflater.inflate(R.layout.cardview2, null); // 2 and 3


        cars.addView(theInflatedView);
        theInflatedView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, FeaturesActivity.class);
                MainActivity.this.startActivity(myIntent);
            }
        });
        Button button = (Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View theInflatedView2 = null;
                if (MainActivity.count%2 == 0)
                    theInflatedView2 = inflater.inflate(R.layout.cardview, null); // 2 and 3
                else
                    theInflatedView2 = inflater.inflate(R.layout.cardview2, null); // 2 and 3
                cars.addView(theInflatedView2);

                theInflatedView2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent myIntent = new Intent(MainActivity.this, FeaturesActivity.class);
                        MainActivity.this.startActivity(myIntent);
                    }
                });
                MainActivity.count ++;
            }
        });


        //RecyclerView recyclerView = (RecyclerView)findViewById(R.id.car_list);
        //ecyclerView.setHasFixedSize(true);
        //LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        //recyclerView.setLayoutManager(mLayoutManager);
        //Car a= new Car("Toyota", "B 10 55");
        //Car a= new Car("Toyota", "B 10 55");
        //Car b = new Car("Bugatti", "b dgs fds");
        //ArrayList<Car> myDataset = new ArrayList<Car>();
        //for (int i =0; i<20;i++) {
         //   myDataset.add(a);
          // myDataset.add(b);
        //}

        // specify an adapter (see also next example)
        //mAdapter = new CarListAdapter(this.getApplicationContext(), myDataset);



    }



}
