package com.example.alex.cloudcar;

/**
 * Created by alex on 3/4/17.
 */

public class Car {
    private String name, license;
    public Car(String name, String licence){
        this.name = name;
        this.license = licence;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }
}
